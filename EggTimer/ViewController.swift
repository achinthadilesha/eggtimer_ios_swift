
import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    let eggtime=["Hard":720, "Medium":480, "Soft":300]
    
//    var counter=60;
    var totalTime=0;
    var timePassed=0;
    
    var time=Timer()
    @IBOutlet weak var timeDisplayLabel: UILabel!
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBAction func hardnessSelected(_ sender: UIButton) {
        
        sender.flash()
        
        time.invalidate()
        
        let hardness=sender.currentTitle!
        
        totalTime=eggtime[hardness]!
        
        // progressBar.progress=(counter/1000)
        
        time=Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
    }
    
    
    
    @objc func updateCounter() {
        //example functionality
        if totalTime > timePassed {
//            print("\(counter) seconds")
            timePassed += 1
            
            let progress=Float(timePassed)/Float(totalTime);
            
//            print(progress)
//            print(timePassed)
            
            progressBar.progress=progress
            textLabel.text=""
            timeDisplayLabel.text="\(timePassed) s"
            
            
            print(totalTime)
            print(timePassed)
            
        }
        else{
            textLabel.text="Done"
            timeDisplayLabel.text=""
            progressBar.progress=0
            totalTime=0
            timePassed=0
            
        }
    
    }
    
}



// button animations
// need to put on a seperate file

extension UIButton{
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
    
    pulse.duration = 0.4
    pulse.fromValue = 0.98
    pulse.toValue = 1.0
    pulse.autoreverses = true
    pulse.repeatCount = 5
        pulse.initialVelocity = 0.5
    pulse.damping = 1.0
    layer.add(pulse, forKey: nil)
    }
    
    
    func flash() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.3
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        layer.add(flash, forKey: nil)}
    
}

